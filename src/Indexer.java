

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This terminal application creates an Apache Lucene index in a folder and adds files into this index
 * based on the input of the user.
 */
public class Indexer {
	private static StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_41);

	private IndexWriter writer;
	private ArrayList<File> queue = new ArrayList<File>();


	public static void main(String[] args) throws IOException, XMLStreamException {
		System.out.println("Enter the path where the index will be created: ");

		String indexLocation = null;
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		String s = br.readLine();

		System.out.println("Enter the path of xml document to be indexed: ");
		String xmlDocumentLocation = br.readLine();


		Indexer indexer = null;
		try {
			indexLocation = s;
			indexer = new Indexer(s);
		} catch (Exception ex) {
			System.out.println("Cannot create index..." + ex.getMessage());
			System.exit(-1);
		}



		// First create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		// Setup a new eventReader
		InputStream in = new FileInputStream(xmlDocumentLocation);
		XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
		// Read the XML document
		Item item = null;

		while (eventReader.hasNext()) {
			XMLEvent event = eventReader.nextEvent();

			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				// If we have a item element we create a new item
				if (startElement.getName().getLocalPart() == ("doc")) {
					item = new Item();
					continue;
				}
			}

			if (item == null)
			{
				// We are not including this document
				continue;
			}

			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart()
						.equals("title")) {
					event = eventReader.nextEvent();
					item.setTitle(event.asCharacters().getData());
					continue;
				}
			}

			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart()
						.equals("abstract")) {
					event = eventReader.nextEvent();
					if (!event.isCharacters()){
						// Empty abstract, we don't want this document
						item = null;
						continue;
					}
					item.setText(event.asCharacters().getData());
					continue;
				}
			}

			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart()
						.equals("url")) {
					event = eventReader.nextEvent();
					item.setUrl(event.asCharacters().getData());
					continue;
				}}

			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart()
						.equals("anchor")) {
					event = eventReader.nextEvent();
					if (event.isCharacters())
						item.setTopics(item.getTopics() +", " + event.asCharacters().getData());
					continue;
				}
			}


			// If we reach the end of an item element we add it to the list
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				if (endElement.getName().getLocalPart() == ("doc")) {
					System.out.println("Indexing: " + item);
					indexer.indexDocument(item);		        	  
					item = null;
				}
			}

		}

		indexer.closeIndex();



	}

	/**
	 * Constructor
	 * @param indexDir the name of the folder in which the index should be created
	 * @throws java.io.IOException when exception creating index.
	 */
	Indexer(String indexDir) throws IOException {
		// the boolean true parameter means to create a new index everytime, 
		// potentially overwriting any existing files there.
		FSDirectory dir = FSDirectory.open(new File(indexDir));
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_41, analyzer);
		writer = new IndexWriter(dir, config);
	}

	/**
	 * @throws java.io.IOException when exception
	 */
	public void indexDocument(Item wikiDocument) throws IOException
	{
		Document doc = new Document();
		doc.add(new TextField("title", wikiDocument.getTitle(),  Field.Store.YES));
		doc.add(new TextField("text", wikiDocument.getText(),  Field.Store.NO));
		doc.add(new TextField("url", wikiDocument.getUrl(),  Field.Store.YES));
		doc.add(new StringField("urlString", wikiDocument.getUrl(),  Field.Store.YES));
		doc.add(new TextField("topics", wikiDocument.getTopics(),  Field.Store.YES));
		writer.addDocument(doc);
	}

	/**
	 * Close the index.
	 * @throws java.io.IOException when exception closing
	 */
	public void closeIndex() throws IOException {
		writer.close();
	}
}