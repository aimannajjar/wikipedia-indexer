public class Item {
	private String title = ""; 
	private String url = "";
	private String text = "";
	private String topics = "";

	@Override
	public String toString() {
		return "Item [" + title + " (" + url + ")]";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title.replaceFirst("Wikipedia: ", "");
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTopics() {
		return topics;
	}

	public void setTopics(String topics) {
		this.topics = topics;
	}
} 